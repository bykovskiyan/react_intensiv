import React from 'react';
import style from './content.module.scss';

function Content( { contentText } ) {
  //let contentText = 'Вторая домашняя работа';
  return (
    <div className={ style.content }>
      <div className={ style.wrapper }>
        { contentText }
      </div>
    </div>
  );
}

export default Content;
