import React from 'react';
import style from './footer.module.scss';

function Footer( { footerText } ) {
  return (
    <div className={ style.footer }>
      <div className={ style.wrapper }>
        { footerText }
      </div>
    </div>
  );
}

export default Footer;
