import React from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';
import Content from '../content/content';
import style from './app.module.scss';

function App() {
  return (
    <div className={ style.app }>
      <Header logoText = 'React marathon' sloganText = 'марафон по программированию на javascript с использованием библиотеки react.js' />
      <Content contentText='Вторая домашняя работа' />
      <Footer footerText='2020 &copy; React marathon. All rights recerved.' />
    </div>
  );
}

export default App;
