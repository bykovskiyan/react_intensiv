import React from 'react';
import style from './header.module.scss';
import { ReactComponent as SVGlogo } from './img/react_icon.svg';

function Header( { logoText, sloganText } ) {
  //let logoText = 'React marathon';
  //let sloganText = 'марафон по программированию на javascript с использованием библиотеки react.js';
  return (
    <div className={ style.header }>
      <div className={ style.wrapper }>
        <div className={ style.logo }>
          <div className={ style.img }><SVGlogo/></div>
          <div className={ style.text }>{ logoText }</div>
        </div>
        <div className={ style.slogan }>{ sloganText }</div>
      </div>
    </div>
  );
}

export default Header;
